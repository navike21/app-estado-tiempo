import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  // Link
} from 'react-router-dom';

import Dashboard from './dashboard/dashboard';
import Configuracion from './dashboard/configuracion';
import {
  userSettings
} from './config/general';
import GetInfo from './getInfo/GetInfo';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route 
    {...rest} 
    render={(props) => (
      userSettings.sessionState
      ? <Component {...props} />
      : <Redirect to={{
        pathname: '/getinfo',
        state: { from: props.location }
    }} />
  )} />
);

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/getinfo" component={GetInfo} />
        <PrivateRoute path="/config" component={Configuracion} />
        <PrivateRoute path="/" component={Dashboard} />
      </Switch>
    </Router>
  );
};

export default App