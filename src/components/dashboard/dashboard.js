import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';
import "moment/locale/es";
import "../../components/assets/sass/dashboard/dashboard.sass";
import { userSettings, imagenesPrecarga } from "../config/general";
import { fadeOutImportant } from "../config/funciones";
import {urlEndpoints, apiKey, urlGeoLocalize, apiKeyGeo} from '../config/general';

//icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog, faWind, faSun, faCloud, faTint } from "@fortawesome/free-solid-svg-icons";

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionState: userSettings.sessionState,
      amanecer: "00:00:00",
      atardecer: "00:00:00",
      diaEstado: "",
      latitud: 0,
      longitud: 0,
      unidadTemperatura: "c",
      temperatura: 0,
      sunrise: 0,
      sunset: 0,
      windSpeed: 0,
      uvi: 0,
      cloud: 0,
      humedad: 0,
      sensacionTermica: 0,
      presionAdmosferica: 0,
      city: "cargando ciudad",
    };
  };

  fnEstadoDia = () => {
    const hora = parseInt(moment().format("HH"));
    let estadoDia = '';
    if (hora > 18 && hora <= 24) {
      estadoDia = "noche";
    } else if (hora >= 0 && hora <= 5) {
      estadoDia = "noche";
    } else if (hora > 5 && hora <= 9) {
      estadoDia = "amanece";
      // estadoDia = "";
    } else if (hora > 9 && hora <= 15) {
      estadoDia = "dia";
    } else if (hora > 15 && hora <= 18) {
      estadoDia = "atardece";
    }
    return estadoDia;
  }

  fnTiempoAmanecer = (h, m, s, n) => {
    const hora = h;
    const minuto = m;
    const segundo = s;
    const noche = n;
    if (hora >= 0) {
      this.setState({
        amanecer: `${String(hora).length === 1 ? "0" + hora : hora}:${ String(minuto).length === 1 ? "0" + minuto : minuto }:${String(segundo).length === 1 ? "0" + segundo : segundo}`,
        // diaEstado: this.fnEstadoDia()
        diaEstado: noche ? this.fnEstadoDia() : 'amanece'
      });
    }
  };

  fnTiempoAtardecer = (h, m, s, n) => {
    const hora = h;
    const minuto = m;
    const segundo = s;
    const noche = n;

    this.setState({
      atardecer: `${String(hora).length === 1 ? "0" + hora : hora}:${ String(minuto).length === 1 ? "0" + minuto : minuto }:${String(segundo).length === 1 ? "0" + segundo : segundo}`,
      // diaEstado: this.fnEstadoDia()
      diaEstado: !noche ? this.fnEstadoDia() : 'atardece'
    });
  };

  fnAmanece = () => {
    const amanece = this.fnTiempoAmanecer;
    const amaneceInterval = setInterval(() => {
      const testDate = this.state.sunrise;
      if (testDate) {
        const now = moment();
        const amanecer = moment.unix(testDate);
        const restante = amanecer.diff(now, "milliseconds");
        const d = moment.duration(restante, "milliseconds");
        const horas = Math.floor(d.asHours());
        const minutos = Math.floor(d.asMinutes() - horas * 60);
        const segundos = Math.floor((d / 1000) % 60);
        const noche = now.isBefore(amanecer);

        if (horas >= 0){
          amanece(horas, minutos, segundos, noche);
        } else {
          clearInterval(amaneceInterval);
        }
      }
    }, 1000);
  }

  fnAtardece = () => {
    const atardece = this.fnTiempoAtardecer;
    const atardeceInterval = setInterval(() => {
      const testDate = this.state.sunset;
      if (testDate) {
        const now = moment();
        const atardecer = moment.unix(testDate);
        const restante = atardecer.diff(now, "milliseconds");
        const d = moment.duration(restante, "milliseconds");
        const horas = Math.floor(d.asHours());
        const minutos = Math.floor(d.asMinutes() - horas * 60);
        const segundos = Math.floor((d / 1000) % 60);
        const noche = !now.isBefore(atardecer);
        
        if (horas >= 0) {
          atardece(horas, minutos, segundos, noche);
        } else {
          clearInterval(atardeceInterval);
        }
      }
    }, 1000);
  }

  fnInsertImageFondo = () => {
    const imagenStado = document.getElementsByClassName("imagenStado")[0];
    if (imagenStado) {
      const imagenMountain = imagenesPrecarga[0].value;
      imagenMountain.classList.add("mountain");
      imagenStado.appendChild(imagenMountain);
    }
  };

  // Obtener Coordenadas
  fnGetCoordenadas = (fn) => {
    const funcion = fn;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.setState({
          latitud: position.coords.latitude,
          longitud: position.coords.longitude,
        });
        const urlEndpointGeo = `${urlGeoLocalize}?q=${this.state.latitud}+${this.state.longitud}&key=${apiKeyGeo}`;
        fetch(urlEndpointGeo)
          .then((response) => {
            return response.json();
          })
          .then((json) => {
            this.setState({
              city: json.results[0].components.city,
            });
            funcion();
          });
      });
    } else {
      this.setState({
        latitud: -12.02,
        longitud: -77.26,
      });
      funcion();
    }
  };

  // LLAMADAS CON EL API

  fnGetUbicacion = () => {
    // Ubicación
    if (this.state.latitud) {
      const urlEndpointGeo = `${urlGeoLocalize}?q=${this.state.latitud}+${this.state.longitud}&key=${apiKeyGeo}`;
      fetch(urlEndpointGeo)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          this.setState({
            city: json.results[0].components.city
          })
        });
    }
  };

  updateInfoPrincipal = (t, sr, st, ws, uvi, cloud, hum, ster, pa) => {
    const temperatura = t;
    const sunrise = sr;
    const sunset = st;
    const windSpeed = ws;
    const uv = uvi;
    const nubes = cloud;
    const humedad = hum;
    const sensacionTermica = ster;
    const presionAdmosferica = pa;
    
    this.setState({
      temperatura: temperatura,
      sunrise: sunrise,
      sunset: sunset,
      windSpeed: windSpeed,
      uvi: uv,
      cloud: nubes,
      humedad: humedad,
      sensacionTermica: sensacionTermica,
      presionAdmosferica: presionAdmosferica, 
    });
  };

  fnGetInfoApiDefault = () => {
    if (this.state.latitud) {
      const urlApi = urlEndpoints;
      const ApiKey = apiKey;
      const units = this.state.unidadTemperatura === 'c' ? 'metric' : 'imperial';
      const urlEndpoint = `${urlApi}/onecall?lat=${String(this.state.latitud)}&lon=${String(this.state.longitud)}&units=${units}&appid=${ApiKey}`;
      // https://api.openweathermap.org/data/2.5/onecall?lat=-12.04318&lon=-77.02824&exclude=hourly,daily&appid=daf6d7594db5bd0dc80b220fa889218b
  
      fetch(urlEndpoint)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          const updateInfo = this.updateInfoPrincipal;
  
          const temperatura = json.current.temp || 'no data';
          const sunrise = json.current.sunrise || "no data";
          const sunset = json.current.sunset || "no data";
          const windSpeed = json.current.wind_speed || "no data";
          const uv = json.current.uvi || "no data";
          const cloud = json.current.clouds || "no data";
          const humidity = json.current.humidity || "no data";
          const sensacionTermica = json.current.feels_like || "no data";
          const presionAdmosferica = json.current.pressure || "no data";
          updateInfo(
            temperatura,
            sunrise,
            sunset,
            parseInt(windSpeed) * 3.6,
            parseFloat(uv).toFixed(1),
            cloud,
            humidity,
            sensacionTermica,
            presionAdmosferica
          );
        });
    }
  };
  
  componentDidMount = () => {
    moment.locale("es");
    fadeOutImportant();
    this.fnInsertImageFondo();
    this.fnGetCoordenadas(this.fnGetInfoApiDefault);
    this.fnAmanece();
    this.fnAtardece();
  };

  render() {
    return (
      <div className="dashboard">
        <div className={`imagenStado ${this.state.diaEstado}`}>
          <Link className="config" to="/config">
            {/* <FontAwesomeIcon icon={faCog} /> */}
          </Link>
          <div className="temperatura">{this.state.temperatura}°</div>
          <div className="ubicacion">{this.state.city}</div>
          <div className="fecha">
            {moment().format("dddd DD [de] MMMM YYYY, h:mm:ss a")}
          </div>
          <ul className="infoPrincipal">
            <li>
              <FontAwesomeIcon icon={faWind} />
              <span> {this.state.windSpeed} km/h</span>
            </li>
            <li>
              <FontAwesomeIcon icon={faSun} />
              <span> {this.state.uvi} UV</span>
            </li>
            <li>
              <FontAwesomeIcon icon={faCloud} />
              <span> {this.state.cloud}%</span>
            </li>
            <li>
              <FontAwesomeIcon icon={faTint} />
              <span> {this.state.humedad}%</span>
            </li>
          </ul>
          <ul className="informacionAdicional">
            <li>Sensación Térmica: {parseInt(this.state.sensacionTermica)}°</li>
            <li>Presión Admosférica: {this.state.presionAdmosferica}hPA</li>
          </ul>
          <ul className="infoAmaneceAtardece">
            {this.state.amanecer === "00:00:00" ? (
              ""
            ) : (
              <li>Amanece en: {this.state.amanecer}</li>
            )}
            {this.state.atardecer === "00:00:00" ? (
              ""
            ) : (
              <li>Atardece en: {this.state.atardecer}</li>
            )}
          </ul>
        </div>
        {/* Atardece en {this.state.atardecer}
        Amanece en {this.state.amanecer} */}
        {/* // Coordenadas {this.state.latitud} y {this.state.longitud} */}
      </div>
    );
  }
}
