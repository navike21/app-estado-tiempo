import React, { Component } from 'react';
import {
  userSettings
} from '../config/general';
import {
  Redirect
} from 'react-router-dom';
import { setSession, focalizaImportantBackground } from '../config/funciones';

import '../../components/assets/sass/main.sass';
import '../../components/assets/sass/getinfo/getinfo.sass';

import imagesSvg from '../config/imagesSvg';

export default class GetInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionState: userSettings.sessionState,
      nameStatus: true,
      generoStatus: true
    }
  };

  updateSession = () => {
    // Informacion recogida del formulario

    const nombres = document.getElementById('nombreInfo').value;
    const genero = document.getElementById('genero').value;

    if (nombres && genero) {
      this.setState({
        sessionState: !this.state.sessionState,
        nameStatus: true,
        generoStatus: true
      });
      userSettings.nameUser = nombres;
      userSettings.gender = genero;
      userSettings.sessionState = true;
      setSession(userSettings);
    } else {
      nombres ? this.setState({ nameStatus: true }) : this.setState({ nameStatus: false });
      genero ? this.setState({ generoStatus: true }) : this.setState({ generoStatus: false });
    }
  };

  componentDidMount = () => {
    const seleccionaGenero = document.getElementsByClassName('seleccionaGenero')[0];
    if (seleccionaGenero) {
      const span = seleccionaGenero.getElementsByTagName('span')[0];
      if (span) {
        span.innerHTML = imagesSvg.triangulo
      }
    }
  }

  componentWillMount = () => {
    setTimeout(() => {
      focalizaImportantBackground();
    }, 1000);
  };

  render() {
    return (
      <div className="getInfoWrapp">
        <h2 className="titulo">¿Nos conocemos?</h2>
        <h4 className="subtitulo">Ingrese sus datos</h4>
        <input type="text" name="nombre" id="nombreInfo" placeholder="Nombres" className={!this.state.nameStatus ? 'errorInput' : ''} />
        <div className="seleccionaGenero">
          <span></span>
          <select name="genero" id="genero" className={!this.state.generoStatus ? 'errorInput' : ''}>
            <option value="">Seleccione su sexo</option>
            <option value="hombre">Hombre</option>
            <option value="mujer">Mujer</option>
          </select>
        </div>
        <button onClick={this.updateSession}>
          Acceder
        </button>
        {this.state.sessionState ? <Redirect push to="/" /> : ""}
      </div>
    );
  }
}
