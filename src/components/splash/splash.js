import React, { Component } from 'react';
import * as ImagePreloader from 'image-preloader';
import '../../components/assets/sass/splash/splash.sass';
import {
  imagenesPrecarga
} from '../config/general';
import images from '../config/images';
import {
  createElement
} from '../config/funciones';
import Main from '../Main';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalPorcentaje: 0,
      complete: false
    }
  };

  updateProgress = (number) => {
    const progreso = number;
    this.setState({ totalPorcentaje: progreso });
  };

  showBlurBackground = (getImg) => {
    const imagenBlur = getImg;
    imagenBlur.classList.add('imgBg');
    const bodyTag = document.getElementsByTagName('body')[0];
    if (bodyTag) {
      bodyTag.appendChild(imagenBlur);
    }
  };

  showLogoImage = (getImg) => {
    const imagenLogo = getImg;
    const textoLoading = createElement('span', 'textoLoading');
    textoLoading.innerText = 'cargando recursos';
    const logoSplash = document.getElementsByClassName('logoSplash')[0];
    const progressBarContent = document.getElementsByClassName('progressBarContent')[0];
    const porcentaje = document.getElementsByClassName('porcentaje')[0];
    if (logoSplash) {
      logoSplash.appendChild(imagenLogo);
      logoSplash.appendChild(textoLoading);
      logoSplash.classList.add('fadeInDown');
      progressBarContent.classList.add('fadeInUp');
      porcentaje.classList.add('fadeIn');
    }
  };

  removeLoad = () => {
    const splash = document.getElementsByClassName('splash')[0];
    setTimeout(() => {
      if (splash) {
        splash.classList.add("fadeOut");
      }
    }, 2000);
    setTimeout(() => {
      if (splash) {
        splash.remove();
        this.setState({ complete: true });
      }
    }, 2500);
    return '';
  };

  componentDidMount = () => {
    const totalLoad = images.length;
    let progreso = 0;
    let porcentaje = 0;
    var preloader = new ImagePreloader();
    
    const fnUpdate = this.updateProgress;
    const fnBlurBackground = this.showBlurBackground;
    const fnLogo = this.showLogoImage;

    preloader.onProgress = (info) => {
      let multiplicacion = 0;
      progreso = Number(progreso) + 1;
      const srcImagen = info.value.getAttribute('src');
      if (srcImagen.includes('background.')) {
        fnBlurBackground(info.value);
      }
      if (srcImagen.includes('logo')) {
        fnLogo(info.value);
      }
      multiplicacion = Number(progreso) * 100;
      porcentaje = multiplicacion / totalLoad;
      fnUpdate(porcentaje);
    };

    preloader.preload(images)
    .then((status) => {
      for (let i = 0; i < status.length; i += 1) {
        imagenesPrecarga.push(status[i]);
      }
    });
  };

  render() { 
    return (
      <div>
        <div
          className={`splash ${
            this.state.totalPorcentaje === 100 ? `${this.removeLoad()}` : ""
          }`}
        >
          <div className="logoSplash"></div>
          <div className="progressBarContent">
            <div
              className="progressBar"
              style={{ width: `${this.state.totalPorcentaje}%` }}
            ></div>
          </div>
          <div className="porcentaje">
            {parseInt(this.state.totalPorcentaje)}%
          </div>
        </div>
        {this.state.complete ? <Main /> : ""}
      </div>
    );
  }
}

export default Splash;
