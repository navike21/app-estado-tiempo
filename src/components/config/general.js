import {
  getSession,
  getNameSession,
  getGenderSession
} from './funciones';

export const pageSettings = {};

export const userSettings = {
  nameUser: getNameSession(),
  gender: getGenderSession(),
  sessionState: getSession()
};

export const imagenesPrecarga = [];

export const urlEndpoints = 'https://api.openweathermap.org/data/2.5';
export const apiKey = 'daf6d7594db5bd0dc80b220fa889218b';

export const urlGeoLocalize = 'https://api.opencagedata.com/geocode/v1/json';
export const apiKeyGeo = '3d334d9bff4e42d7aafea06d96628481';
