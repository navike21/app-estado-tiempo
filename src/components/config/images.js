import background from '../assets/images/background.jpg';
import logo from '../assets/images/logo.svg';
import mountain from '../assets/images/mountain.svg';

const images = [
  mountain,
  logo,
  background
];

export default images;