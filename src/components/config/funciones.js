export const createElement = (elemento, clase, id) => {
  const element = document.createElement(elemento);
  if (id) {
    element.setAttribute('id', id);
  }
  if (clase) {
    if (typeof (clase) === 'string') {
      element.classList.add(clase);
    }
    if (typeof (clase) === 'object') {
      clase.forEach((clss) => {
        element.classList.add(clss);
      });
    }
  }
  return element;
};

export const setSession = (string) => {
  const token = JSON.stringify(string);
  sessionStorage.setItem('sessionObject', token);
};

const infoSession = () => {
  const info = sessionStorage.getItem('sessionObject');
  const retorno = info ? JSON.parse(info) : '';
  return retorno;
};

export const getSession = () => {
  const retorno = infoSession() ? infoSession().sessionState : false
  return retorno;
};

export const getNameSession = () => {
  const retorno = infoSession() ? infoSession().nameUser : '';
  return retorno;
};

export const getGenderSession = () => {
  const retorno = infoSession() ? infoSession().gender : '';
  return retorno;
};

export const focalizaImportantBackground = () => {
  const imgBg = document.getElementsByClassName('imgBg')[0];
  if (imgBg) {
    imgBg.classList.add('focalizaImportantBackground');
  }
};

export const fadeOutImportant = () => {
  const imgBg = document.getElementsByClassName("imgBg")[0];
  if (imgBg) {
    imgBg.classList.add("fadeOutImportant");
  }
};
